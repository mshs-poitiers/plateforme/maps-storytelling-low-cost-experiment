# Maps Storytelling low-cost experiment

Preuve de concept d'utilisation de Quarto & RevealJs pour la fabrication de StoryMaps

- Consulter le [code source](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/maps-storytelling-low-cost-experiment/-/blob/main/src/index.qmd)

- Consulter le [site web généré](https://mshs-poitiers.gitpages.huma-num.fr/plateforme/maps-storytelling-low-cost-experiment/)
